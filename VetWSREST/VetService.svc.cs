﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using VetWSREST.Persistencia;
using VetWSREST.Dominio;
using System;

namespace VetWSREST
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "VetService" en el código, en svc y en el archivo de configuración a la vez.
    // NOTA: para iniciar el Cliente de prueba WCF para probar este servicio, seleccione VetService.svc o VetService.svc.cs en el Explorador de soluciones e inicie la depuración.
    [System.ServiceModel.ServiceBehavior(IncludeExceptionDetailInFaults = true)]
    public class VetService : IVetService
    {

        private VentaDAO ventaDAO = new VentaDAO();
        private VentaDetalleDAO ventadetalleDAO = new VentaDetalleDAO();
     
        public Ventas CrearVenta(Ventas ventaCrear)
        {
            return ventaDAO.CrearVenta(ventaCrear);
        }

        public Ventas ObtenerVenta(string codigo)
        {
            return ventaDAO.ObtenerVenta(codigo);
        }

        public List<Ventas> ListarVenta()
        {
            return ventaDAO.ListarVenta();
        }


        public Ventas ModificarVenta(Ventas ventaModificar)
        {
            return ventaDAO.ModificarVenta(ventaModificar);
        }
   
        public VentaDetalles CrearDetalleVenta(VentaDetalles ventadetalleCrear)
        {
            return ventadetalleDAO.CrearVentaDetalle(ventadetalleCrear);
        }

        public VentaDetalles ObtenerDetalleVenta(string codigo)
        {
            return ventadetalleDAO.ObtenerVentaDetalle(Convert.ToInt32(codigo));
        }

        public List<VentaDetalles> ListarDetalleVenta(string codigo)
        {
            return ventadetalleDAO.ListarVentaDetalle(Convert.ToInt32(codigo));
        }

        public VentaDetalles ModificarDetalleVenta(VentaDetalles ventadetalleModificar)
        {
            return ventadetalleDAO.ModificarVentaDetalle(ventadetalleModificar);
        }

        public void EliminarDetalleVenta(string idventadelle)
        {
             ventadetalleDAO.Eliminar(Convert.ToInt32(idventadelle));
        }

    }
}
