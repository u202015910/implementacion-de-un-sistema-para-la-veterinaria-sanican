﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using VetWSREST.Dominio;

namespace VetWSREST.Persistencia
{
    public class VentaDAO
    {
        private string CadenaConexion = "Data Source=(local); Initial Catalog=VetDSD; Integrated Security=SSPI";

        public Ventas CrearVenta(Ventas ventaACrear)
        {

            Ventas VentaReg = null;

            string sql1 = "insert into venta(IDCliente, Fecha, IDUsuario, CodigoVenta) " +
                "values (@idcliente, @fecha, @idusuario, @codigoventa); " +
                "select cast(scope_identity() as int)";

            using (SqlConnection conexion = new SqlConnection(CadenaConexion))
            {
                conexion.Open();
                using (SqlCommand comando = new SqlCommand(sql1, conexion))
                {
                    comando.Parameters.Add(new SqlParameter("@idcliente", ventaACrear.idcliente));
                    comando.Parameters.Add(new SqlParameter("@fecha", ventaACrear.fecha));
                    comando.Parameters.Add(new SqlParameter("@idusuario", ventaACrear.idusuario));
                    comando.Parameters.Add(new SqlParameter("@codigoventa", ventaACrear.codigoventa));
                    comando.ExecuteNonQuery();
                }
                //ClienteReg = Obtener(ID);
                //return ClienteReg;
                VentaReg = ObtenerVenta(ventaACrear.codigoventa);
                return VentaReg;
            }

        }

        public Ventas ObtenerVenta(string codigoventa)
        {
            Ventas ventaEncontrado = null;
            string sql = "select * from venta where codigoventa = @codigoventa";

            using (SqlConnection conexion = new SqlConnection(CadenaConexion))
            {
                conexion.Open();
                using (SqlCommand comando = new SqlCommand(sql, conexion))
                {
                    comando.Parameters.Add(new SqlParameter("@codigoventa", codigoventa));

                    using (SqlDataReader resultado = comando.ExecuteReader())
                    {
                        if (resultado.Read())
                        {
                            ventaEncontrado = new Ventas()
                            {
                                idVenta = (int)resultado["idventa"],
                                idcliente = (int)resultado["idcliente"],
                                fecha = (DateTime)resultado["fecha"],
                                idusuario = (int)resultado["idusuario"],
                                codigoventa = (string)resultado["codigoventa"]
                            }; 
                        }
                    }
                }
                return ventaEncontrado;
            }
        }

        public List<Ventas> ListarVenta()
        {
            List<Ventas> ventaEncontrados = new List<Ventas>();
            Ventas ventaEncontrado = null;


            string sql = "select * from venta";

            using (SqlConnection conexion = new SqlConnection(CadenaConexion))
            {
                conexion.Open();
                using (SqlCommand comando = new SqlCommand(sql, conexion))
                {

                    using (SqlDataReader resultado = comando.ExecuteReader())
                    {
                        while (resultado.Read())
                        {
                            ventaEncontrado = new Ventas()
                            {
                                idVenta = (int)resultado["idventa"],
                                idcliente = (int)resultado["idcliente"],
                                idusuario = (int)resultado["idusuario"],
                                fecha = (DateTime)resultado["fecha"],
                                codigoventa = (string)resultado["codigoventa"]
                            };

                            ventaEncontrados.Add(ventaEncontrado);
                        }
                    }
                }
            }
            return ventaEncontrados;

        }


        public Ventas ModificarVenta(Ventas ventaAModificar)
        {

            Ventas VentaReg = null;

            string sql1 = "update venta set IDCliente = @idcliente, Fecha = @fecha, IDUsuario = @idusuario, CodigoVenta =@codigoventa  " +
                "where idventa = @idventa ; " +
                "select cast(scope_identity() as int)";

            using (SqlConnection conexion = new SqlConnection(CadenaConexion))
            {
                conexion.Open();
                using (SqlCommand comando = new SqlCommand(sql1, conexion))
                {
                    comando.Parameters.Add(new SqlParameter("@idventa", ventaAModificar.idVenta));
                    comando.Parameters.Add(new SqlParameter("@idcliente", ventaAModificar.idcliente));
                    comando.Parameters.Add(new SqlParameter("@fecha", ventaAModificar.fecha));
                    comando.Parameters.Add(new SqlParameter("@idusuario", ventaAModificar.idusuario));
                    comando.Parameters.Add(new SqlParameter("@codigoventa", ventaAModificar.codigoventa));
                    comando.ExecuteNonQuery();
                }
                //ClienteReg = Obtener(ID);
                //return ClienteReg;
                VentaReg = ObtenerVenta(ventaAModificar.codigoventa);
                return VentaReg;
            }

        }


    }
}