﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using VetWSREST.Dominio;

namespace VetWSREST.Persistencia
{
    public class VentaDetalleDAO
    {

        private string CadenaConexion = "Data Source=(local); Initial Catalog=VetDSD; Integrated Security=SSPI";

        public VentaDetalles CrearVentaDetalle(VentaDetalles ventadelalleACrear)
        {

            VentaDetalles VentaReg = null;

            string sql1 = "insert into ventadetalle(idventa, idproducto, descripcion, cantidad,preciounit,total) " +
                "values (@idventa, @idproducto, @descripcion, @cantidad,@preciounit,@total); " +
                "select cast(scope_identity() as int)";

            using (SqlConnection conexion = new SqlConnection(CadenaConexion))
            {
                conexion.Open();
                using (SqlCommand comando = new SqlCommand(sql1, conexion))
                {
                    comando.Parameters.Add(new SqlParameter("@idventa", ventadelalleACrear.idVenta));
                    comando.Parameters.Add(new SqlParameter("@idproducto", ventadelalleACrear.idProducto));
                    comando.Parameters.Add(new SqlParameter("@descripcion", ventadelalleACrear.descripcion));
                    comando.Parameters.Add(new SqlParameter("@cantidad", ventadelalleACrear.cantidad));
                    comando.Parameters.Add(new SqlParameter("@preciounit", ventadelalleACrear.preciounit));
                    comando.Parameters.Add(new SqlParameter("@total", ventadelalleACrear.total));
                    comando.ExecuteNonQuery();
                }
                //ClienteReg = Obtener(ID);
                //return ClienteReg;
                VentaReg = ObtenerVentaDetalle(ventadelalleACrear.idVentaDetalle);
                return VentaReg;
            }

        }

        public VentaDetalles ObtenerVentaDetalle(int idventadetalle)
        {
            VentaDetalles ventaEncontrado = null;
            string sql = "select * from ventadetalle where idventadetalle = @idventadetalle";

            using (SqlConnection conexion = new SqlConnection(CadenaConexion))
            {
                conexion.Open();
                using (SqlCommand comando = new SqlCommand(sql, conexion))
                {
                    comando.Parameters.Add(new SqlParameter("@idventadetalle", idventadetalle));

                    using (SqlDataReader resultado = comando.ExecuteReader())
                    {
                        if (resultado.Read())
                        {
                            ventaEncontrado = new VentaDetalles()
                            {
                                idVenta = (int)resultado["idVenta"],
                                idVentaDetalle = (int)resultado["idVentaDetalle"],
                                idProducto = (int)resultado["idProducto"],
                                cantidad = (double)resultado["cantidad"],
                                descripcion= (string)resultado["descripcion"],
                                preciounit = (decimal)resultado["preciounit"],
                                total = (decimal)resultado["total"]

                            };
                        }
                    }
                }
                return ventaEncontrado;
            }
        }

        public List<VentaDetalles> ListarVentaDetalle(int idventa)
        {
            List<VentaDetalles> ventaEncontrados = new List<VentaDetalles>();
            VentaDetalles ventaEncontrado = null;


            string sql = "select * from ventadetalle where idventa = @idventa";

            using (SqlConnection conexion = new SqlConnection(CadenaConexion))
            {
                conexion.Open();
                using (SqlCommand comando = new SqlCommand(sql, conexion))
                {
                    comando.Parameters.Add(new SqlParameter("@idventa", idventa));

                    using (SqlDataReader resultado = comando.ExecuteReader())
                    {
                        while (resultado.Read())
                        {
                            ventaEncontrado = new VentaDetalles()
                            {
                                idVenta = (int)resultado["idVenta"],
                                idVentaDetalle = (int)resultado["idVentaDetalle"],
                                idProducto = (int)resultado["idProducto"],
                                cantidad = (double)resultado["cantidad"],
                                descripcion = (string)resultado["descripcion"],
                                preciounit = (decimal)resultado["preciounit"],
                                total = (decimal)resultado["total"]
                            };

                            ventaEncontrados.Add(ventaEncontrado);
                        }
                    }
                }
            }
            return ventaEncontrados;

        }

        public VentaDetalles ModificarVentaDetalle(VentaDetalles ventadelalleAModificar)
        {

            VentaDetalles VentaReg = null;

            string sql1 = "update ventadetalle set idventa=@idventa, idproducto=@idproducto, descripcion=@descripcion "+
                ", cantidad = @cantidad,preciounit = @preciounit,total = @total where idventadetalle = @idventadetalle " +
                "select cast(scope_identity() as int)";

            using (SqlConnection conexion = new SqlConnection(CadenaConexion))
            {
                conexion.Open();
                using (SqlCommand comando = new SqlCommand(sql1, conexion))
                {
                    comando.Parameters.Add(new SqlParameter("@idventadetalle", ventadelalleAModificar.idVentaDetalle));
                    comando.Parameters.Add(new SqlParameter("@idventa", ventadelalleAModificar.idVenta));
                    comando.Parameters.Add(new SqlParameter("@idproducto", ventadelalleAModificar.idProducto));
                    comando.Parameters.Add(new SqlParameter("@descripcion", ventadelalleAModificar.descripcion));
                    comando.Parameters.Add(new SqlParameter("@cantidad", ventadelalleAModificar.cantidad));
                    comando.Parameters.Add(new SqlParameter("@preciounit", ventadelalleAModificar.preciounit));
                    comando.Parameters.Add(new SqlParameter("@total", ventadelalleAModificar.total));
                    comando.ExecuteNonQuery();
                }
                //ClienteReg = Obtener(ID);
                //return ClienteReg;
                VentaReg = ObtenerVentaDetalle(ventadelalleAModificar.idVentaDetalle);
                return VentaReg;
            }

        }


        public void Eliminar(int idventadetalle)
        {
            string sql = "delete from ventadetalle where idventadetalle=@idventadetalle";

            using (SqlConnection conexion = new SqlConnection(CadenaConexion))
            {
                conexion.Open();
                using (SqlCommand comando = new SqlCommand(sql, conexion))
                {
                    comando.Parameters.Add(new SqlParameter("@idventadetalle", idventadetalle));
                    comando.ExecuteNonQuery();
                }
            }

        }

    }
}