﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace VetWSREST.Dominio
{
    [DataContract]
    public class VentaDetalle
    {
        [DataMember]
        public int idVentaDetalle { get; set; }
        [DataMember]
        public int idVenta { get; set; }
        [DataMember]
        public int idProducto { get; set; }
        [DataMember]
        public string descripcion { get; set; }
        [DataMember]
        public decimal cantidad { get; set; }
        [DataMember]
        public decimal total { get; set; }
    }
}