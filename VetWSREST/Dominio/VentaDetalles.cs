﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace VetWSREST.Dominio
{
    public class VentaDetalles
    {
        [DataMember]
        public int idVentaDetalle { get; set; }
        [DataMember]
        public int idVenta { get; set; }
        [DataMember]
        public int idProducto { get; set; }
        [DataMember]
        public string descripcion { get; set; }
        [DataMember]
        public decimal preciounit { get; set; }
        [DataMember]
        public double cantidad { get; set; }
        [DataMember]
        public decimal total { get; set; }
    }
}