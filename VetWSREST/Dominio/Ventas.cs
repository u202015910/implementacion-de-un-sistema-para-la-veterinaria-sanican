﻿using System;
using System.Runtime.Serialization;


namespace VetWSREST.Dominio
{
    //[DataContract]
    public class Ventas
    {
        [DataMember]
        public int idVenta { get; set; }
        [DataMember]
        public int idcliente { get; set; }
        [DataMember]
        public DateTime fecha { get; set; }
        [DataMember]
        public int idusuario { get; set; }
        [DataMember]
        public string codigoventa { get; set; }
    }
}