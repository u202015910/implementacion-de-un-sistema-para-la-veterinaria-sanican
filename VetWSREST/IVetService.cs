﻿using VetWSREST.Dominio;
using VetWSREST.Persistencia;
using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace VetWSREST
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de interfaz "IVetService" en el código y en el archivo de configuración a la vez.
    [ServiceContract]
    public interface IVetService
    {

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "ventasvet/{codigo}", ResponseFormat = WebMessageFormat.Json)]
        Ventas ObtenerVenta(string codigo);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "Ventasvet", ResponseFormat = WebMessageFormat.Json)]
        List<Ventas> ListarVenta();

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "ventasvet", ResponseFormat = WebMessageFormat.Json)]
        Ventas CrearVenta(Ventas VentaCrear);


        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "ventasvet", ResponseFormat = WebMessageFormat.Json)]
        Ventas ModificarVenta(Ventas VentaModificar);


        ///------ Detalle de Ventas 

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "ventas/detalleventas", ResponseFormat = WebMessageFormat.Json)]
        VentaDetalles CrearDetalleVenta(VentaDetalles VentaDetalleCrear);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "detalleventas/{codigo}", ResponseFormat = WebMessageFormat.Json)]
        VentaDetalles ObtenerDetalleVenta(string codigo);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "detalleventasall/{idventa}", ResponseFormat = WebMessageFormat.Json)]
        List<VentaDetalles> ListarDetalleVenta(string idventa);

        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "ventas/detalleventas", ResponseFormat = WebMessageFormat.Json)]
        VentaDetalles ModificarDetalleVenta(VentaDetalles VentaDetalleModificar);

        [OperationContract]
        [WebInvoke(Method = "DELETE", UriTemplate = "ventas/detalleventas/{idventadetalle}", ResponseFormat = WebMessageFormat.Json)]
        void EliminarDetalleVenta(string idventadetalle);






    }
}
