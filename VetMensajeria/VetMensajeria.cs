﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RabbitMQ.Client;
using Newtonsoft.Json;
//using ServicioRest.Model;
using VetMensajeria.VetServiceReference1;

namespace VetMensajeria
{
    class Programa
    {
        static void Main(string[] args)
        {
            var factory = new ConnectionFactory()
            {
                HostName = "buck-01.rmq.cloudamqp.com",
                UserName = "mrcinayj",
                Password = "of2lHl0TqVRkulqmSLcwmbLx4oveClik",
                VirtualHost = "mrcinayj"
            };

            using (var connection = factory.CreateConnection())
            {
                using (var channel = connection.CreateModel())
                {
                    //xxchannel.BasicQos(prefetchSize: 0, prefetchCount: 1, global: false);
                    //xxQueueingBasicConsumer consumer = new QueueingBasicConsumer(channel);
                    channel.QueueDeclare(queue: "janyoza", durable: false,
                        exclusive: false, autoDelete: false, arguments: null
                        );

                    //Ejecutando el SOAP
                    VetServiceClient mensajeCliente = new VetServiceClient("BasicHttpBinding_IVetService");
                    Mensaje xmensaje = new Mensaje();
                    var data = channel.BasicGet("janyoza", false);
                    while (data != null)
                    {
                        var message = Encoding.UTF8.GetString(data.Body.ToArray());


                        //Mensaje xmensaje = new Mensaje();

                        xmensaje = null;
                        xmensaje = JsonConvert.DeserializeObject<Mensaje>(message);

                        //Console.WriteLine("[X] Recibido:{0}", xmensaje.correo);

                        //Console.WriteLine("[X] Recibido:{0}", xmensaje.mensaje1);

                        mensajeCliente.CrearMensaje(xmensaje);

                        channel.BasicAck(data.DeliveryTag, false);
                        data = channel.BasicGet("janyoza", false);

                    } 

                    channel.Close();
                    connection.Close();
                    
                }
                //Console.WriteLine("Presiones Enter para salir.");
                //Console.ReadLine();
            }
        }

    }
}

