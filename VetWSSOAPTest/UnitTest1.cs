﻿using System;
using System.ServiceModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using VetWSSOAPTest.VetWS;


namespace VetWSSOAPTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
       
        public void Test1CrearClienteOk()
        {
            VetWS.VetServiceClient proxy = new VetWS.VetServiceClient();
            VetWS.Cliente clienteCreado = proxy.CrearCliente(new VetWS.Cliente()
            {
                //IDCliente = "x201304562",
                Nombres = "anderson",
                ApellidoPat = "Porras",
                ApellidoMat = "Pimentel",
                TelefonoFijo = "013535027",
                TelefonoMovil = "941490962",
                Correo = "aporras@hotmail.com",
                Direccion = "Mz B",
                IDTipoDocumento = 1,
                Documento = "0017134569",
                FechaNacimiento = DateTime.Parse("2020-06-16"),
                IDUsuario = 1

            });

            Assert.AreEqual("anderson", clienteCreado.Nombres);
            Assert.AreEqual("Porras", clienteCreado.ApellidoPat);
            Assert.AreEqual("Pimentel", clienteCreado.ApellidoMat);
            Assert.AreEqual("013535027", clienteCreado.TelefonoFijo);
            Assert.AreEqual("941490962", clienteCreado.TelefonoMovil);
            Assert.AreEqual("aporras@hotmail.com", clienteCreado.Correo);
            Assert.AreEqual("Mz B", clienteCreado.Direccion);
            Assert.AreEqual(1, clienteCreado.IDTipoDocumento);
            Assert.AreEqual("0017134569", clienteCreado.Documento);
            Assert.AreEqual(DateTime.Parse("2020-06-16"), clienteCreado.FechaNacimiento);
            Assert.AreEqual(1, clienteCreado.IDUsuario);

        }

        [TestMethod]
        public void Test1CrearClienteRepetidoOk()
        {
            VetWS.VetServiceClient proxy = new VetWS.VetServiceClient();
            try
            {
                VetWS.Cliente clienteCreado = proxy.CrearCliente(new VetWS.Cliente()
                {
                    Nombres = "anderson",
                    ApellidoPat = "Porras",
                    ApellidoMat = "Pimentel",
                    TelefonoFijo = "013535027",
                    TelefonoMovil = "941490962",
                    Correo = "aporras@hotmail.com",
                    Direccion = "Mz B",
                    IDTipoDocumento = 1,
                    Documento = "0017134569",
                    FechaNacimiento = DateTime.Parse("2020-06-16"),
                    IDUsuario = 1
                    //FechaNacimiento = "2020-06-16"
                });
            }
            catch (FaultException<VetWS.ErroresException> error)
            {
                Assert.AreEqual("Error al intentar creación cliente", error.Reason.ToString());
                Assert.AreEqual(error.Detail.Codigo, "101");
                Assert.AreEqual(error.Detail.Descripcion, "El cliente YA EXISTE");
            }
        }
    }
}
