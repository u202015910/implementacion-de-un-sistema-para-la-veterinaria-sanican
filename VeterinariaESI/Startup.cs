﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(VeterinariaESI.Startup))]
namespace VeterinariaESI
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
