﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VeterinariaESI.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [HttpPost]
        public ActionResult Index(string correo, string mensaje)
        {
            //Process programa = null;
            //ProcessStartInfo info = null;
            string ruta = "C:\\Users\\Kevin Anyoza\\Documents\\GitHub\\esi4\\VetEnvioMensaje\\bin\\Release\\";
            string ejecutable = Path.Combine(ruta, "VetEnvioMensaje.exe");

            string parametros = correo + " \"" + mensaje + "\"";

            var proc = System.Diagnostics.Process.Start(ejecutable, parametros);
            proc.CloseMainWindow();
            proc.Close();
            //info = new ProcessStartInfo(ExeyParams);
            //programa = Process.Start(info);
            //System.Diagnostics.Process process = new System.Diagnostics.Process();
            //process.StartInfo.FileName = "cmd";

           

            return View();
        }


    }
}