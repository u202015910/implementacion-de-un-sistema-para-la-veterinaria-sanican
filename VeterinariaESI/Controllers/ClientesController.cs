﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using VeterinariaESI.Models;

namespace VeterinariaESI.Controllers
{
    public class ClientesController : Controller
    {

        //private SocratesWS.ve proxy = new SocratesWS.SocratesServiceClient();  
        private VetServiceWSSOAP.VetServiceClient proxy = new VetServiceWSSOAP.VetServiceClient();


        //private VetDSDEntities db = new VetDSDEntities();

        // GET: Clientes
        public ActionResult Index()
        {
            //var cliente = db.Cliente.Include(c => c.TipoDocumento).Include(c => c.Usuario);
            //return View(cliente.ToList());

            VetServiceWSSOAP.Cliente[] clienteCreados = proxy.ListarClientes();
            ViewData["Message"] = "Listado de alumnos";
            ViewBag.Titulo1 = "Codigo Cliente";
            ViewBag.Titulo2 = "Nombre Cliente";
            ViewBag.Titulo3 = "Apellido de Alumno";
            ViewBag.Titulo4 = "DNI de cliente";
            ViewBag.Titulo5 = "Correo de cliente";
            return View(clienteCreados);


        }

        // GET: Clientes/Details/5
        public ActionResult Details(string dni)
        {
            if (dni == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VetServiceWSSOAP.Cliente cliente = proxy.ObtenerCliente(dni);
            if (cliente == null)
            {
                return HttpNotFound();
            }
            ViewData["Message"] = "Detalle del alumno";
            ViewBag.Titulo1 = "Codigo Cliente";
            ViewBag.Titulo2 = "Nombre Cliente";
            ViewBag.Titulo3 = "Apellido de Alumno";
            ViewBag.Titulo4 = "DNI de cliente";
            ViewBag.Titulo5 = "Correo de cliente";
            return View(cliente);
        }

        // GET: Clientes/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IDCliente,Nombres,ApellidoPat,ApellidoMat,TelefonoFijo,TelefonoMovil,Correo,Direccion,IDTipoDocumento,Documento,FechaNacimiento,IDUsuario")] VeterinariaESI.VetServiceWSSOAP.Cliente cliente)
        {
            if (ModelState.IsValid)
            {
                proxy.CrearCliente(cliente);
                return RedirectToAction("Index");
            }

            return View(cliente);
        }

        // POST: Clientes/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Create([Bind(Include = "IDCliente,Nombres,ApellidoPat,ApellidoMat,TelefonoFijo,TelefonoMovil,Correo,Direccion,IDTipoDocumento,Documento,FechaNacimiento,IDUsuario")] Cliente cliente)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Cliente.Add(cliente);
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }

        //    ViewBag.IDTipoDocumento = new SelectList(db.TipoDocumento, "IDTipoDocumento", "Descripcion", cliente.IDTipoDocumento);
        //    ViewBag.IDUsuario = new SelectList(db.Usuario, "IDUsuario", "Nombre", cliente.IDUsuario);
        //    return View(cliente);
        //}

        // GET: Clientes/Edit/5
        //public ActionResult Edit(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    Cliente cliente = db.Cliente.Find(id);
        //    if (cliente == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    ViewBag.IDTipoDocumento = new SelectList(db.TipoDocumento, "IDTipoDocumento", "Descripcion", cliente.IDTipoDocumento);
        //    ViewBag.IDUsuario = new SelectList(db.Usuario, "IDUsuario", "Nombre", cliente.IDUsuario);
        //    return View(cliente);
        //}

        // POST: Clientes/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Edit([Bind(Include = "IDCliente,Nombres,ApellidoPat,ApellidoMat,TelefonoFijo,TelefonoMovil,Correo,Direccion,IDTipoDocumento,Documento,FechaNacimiento,IDUsuario")] Cliente cliente)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Entry(cliente).State = EntityState.Modified;
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }
        //    ViewBag.IDTipoDocumento = new SelectList(db.TipoDocumento, "IDTipoDocumento", "Descripcion", cliente.IDTipoDocumento);
        //    ViewBag.IDUsuario = new SelectList(db.Usuario, "IDUsuario", "Nombre", cliente.IDUsuario);
        //    return View(cliente);
        //}

        // GET: Clientes/Delete/5
        //public ActionResult Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    Cliente cliente = db.Cliente.Find(id);
        //    if (cliente == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(cliente);
        //}

        // POST: Clientes/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public ActionResult DeleteConfirmed(int id)
        //{
        //    Cliente cliente = db.Cliente.Find(id);
        //    db.Cliente.Remove(cliente);
        //    db.SaveChanges();
        //    return RedirectToAction("Index");
        //}

        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        db.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}
    }
}
