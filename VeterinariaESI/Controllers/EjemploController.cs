﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace VeterinariaESI.Controllers
{
    public class EjemploController : Controller
    {
        // GET: Ejemplo
        //privat   proxy = new SocratesWS.SocratesServiceClient();

        //private VetServiceSAOP.VetServiceClient proxy = new VetServiceSAOP.VetServiceClient();

        private VetServiceWSSOAP.VetServiceClient proxy = new VetServiceWSSOAP.VetServiceClient();


        public ActionResult Index()
        {

            string ruta2 = "C:\\Users\\Kevin Anyoza\\Documents\\GitHub\\esi4\\VetMensajeria\\bin\\Release\\";
            string ejecutable2 = Path.Combine(ruta2, "VetMensajeria.exe");

            var proc2 = System.Diagnostics.Process.Start(ejecutable2);
            proc2.CloseMainWindow();
            proc2.Close();

            VetServiceWSSOAP.Mensaje[] mensajesCreados = proxy.ListarMensajes();
            ViewData["Message"] = "Listado de Mensajes";
            ViewBag.Titulo1 = "Codigo";
            ViewBag.Titulo2 = "Correo ";
            ViewBag.Titulo3 = "Mensaje";
           
            return View(mensajesCreados);
        }


        //public descargarmensajes()
        //{ 

        //}


    }
}