﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.IO;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using VeterinariaESI.Models;
using VeterinariaESI.Dominio;

namespace VeterinariaESI.Controllers
{
    public class VentasController : Controller
    {
        private VetDSDEntities db = new VetDSDEntities();

        // GET: Ventas
        //public ActionResult Index()
        //{
        //    var venta = db.Venta.Include(v => v.Cliente).Include(v => v.Usuario);
        //    return View(venta.ToList());
        //}

        // GET: Horarios
        public ActionResult Index()
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.
                    Create("http://localhost:34974/VetService.svc/ventasvet");
            request.Method = "GET";
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            StreamReader reader = new StreamReader(response.GetResponseStream());
            string tramaJson = reader.ReadToEnd();
            JavaScriptSerializer js = new JavaScriptSerializer();
            List<Ventas> ventasObtenidos = js.Deserialize<List<Ventas>>(tramaJson);

            ViewData["Message"] = "Listado de horarios";
            ViewBag.Titulo01 = "idventa ";
            ViewBag.Titulo02 = "idusuario";
            ViewBag.Titulo03 = "fecha";
            ViewBag.Titulo04 = "idcliente";
            ViewBag.Titulo05 = "codigoventa";
    

            return View(ventasObtenidos);
        }

        // GET: Ventas/Details/5
        //public ActionResult Details(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    Venta venta = db.Venta.Find(id);
        //    if (venta == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(venta);
        //}

        // GET: Ventas/Create
        //public ActionResult Create()
        //{
        //    ViewBag.IDCliente = new SelectList(db.Cliente, "IDCliente", "Nombres");
        //    ViewBag.IDUsuario = new SelectList(db.Usuario, "IDUsuario", "Nombre");
        //    return View();
        //}

        // POST: Ventas/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Create([Bind(Include = "IDVenta,IDCliente,Fecha,IDUsuario")] Venta venta)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Venta.Add(venta);
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }

        //    ViewBag.IDCliente = new SelectList(db.Cliente, "IDCliente", "Nombres", venta.IDCliente);
        //    ViewBag.IDUsuario = new SelectList(db.Usuario, "IDUsuario", "Nombre", venta.IDUsuario);
        //    return View(venta);
        //}

        // GET: Ventas/Edit/5
        //public ActionResult Edit(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    Venta venta = db.Venta.Find(id);
        //    if (venta == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    ViewBag.IDCliente = new SelectList(db.Cliente, "IDCliente", "Nombres", venta.IDCliente);
        //    ViewBag.IDUsuario = new SelectList(db.Usuario, "IDUsuario", "Nombre", venta.IDUsuario);
        //    return View(venta);
        //}

        // POST: Ventas/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Edit([Bind(Include = "IDVenta,IDCliente,Fecha,IDUsuario")] Venta venta)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Entry(venta).State = EntityState.Modified;
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }
        //    ViewBag.IDCliente = new SelectList(db.Cliente, "IDCliente", "Nombres", venta.IDCliente);
        //    ViewBag.IDUsuario = new SelectList(db.Usuario, "IDUsuario", "Nombre", venta.IDUsuario);
        //    return View(venta);
        //}

        // GET: Ventas/Delete/5
        //public ActionResult Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    Venta venta = db.Venta.Find(id);
        //    if (venta == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(venta);
        //}

        // POST: Ventas/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public ActionResult DeleteConfirmed(int id)
        //{
        //    Venta venta = db.Venta.Find(id);
        //    db.Venta.Remove(venta);
        //    db.SaveChanges();
        //    return RedirectToAction("Index");
        //}

        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        db.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}
    }
}
