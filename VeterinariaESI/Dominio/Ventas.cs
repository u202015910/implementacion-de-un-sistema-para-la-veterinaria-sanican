﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VeterinariaESI.Dominio
{
    public class Ventas
    {
        public int idventa { get; set; }
        public int idcliente { get; set; }
        public DateTime fecha { get; set; }
        public string codigoventa { get; set; }
        public int idusuario { get; set; }
    }
}