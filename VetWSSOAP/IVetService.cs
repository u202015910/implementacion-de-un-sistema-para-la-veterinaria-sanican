﻿using VetWSSOAP.Dominio;
using VetWSSOAP.Errores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace VetWSSOAP
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de interfaz "IVetService" en el código y en el archivo de configuración a la vez.
    [ServiceContract]
    public interface IVetService
    {
        [FaultContract(typeof(ErroresException))]
        [OperationContract]
        Cliente CrearCliente(Cliente clienteACrear);
        [OperationContract]
        Cliente ObtenerCliente(string documento);
        [OperationContract]
        Cliente ModificarCliente(Cliente clienteAModificar);
        [OperationContract]
        void EliminarCliente(int IDCliente);
        [OperationContract]
        List<Cliente> ListarClientes();



        [OperationContract]
        Mascota CrearMascota (Mascota mascotaACrear);
        [OperationContract]
        Mascota ObtenerMascota(int IDMascota);
        [OperationContract]
         Mascota ModificarMascota(Mascota mascotaAModificar);
        [OperationContract]
        void EliminarMascota(int IDMascota);
        [OperationContract]
        List<Mascota> ListarMascotas();


        [OperationContract]
        Mensaje CrearMensaje(Mensaje mensajeACrear);
        [OperationContract]
        Mensaje ObtenerMensaje(int ID);
        [OperationContract]
        List<Mensaje> ListarMensajes();

    }
}
