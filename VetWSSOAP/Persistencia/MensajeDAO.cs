﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using VetWSSOAP.Dominio;

namespace VetWSSOAP.Persistencia
{
    public class MensajeDAO
    {

        private string CadenaConexion = "Data Source=(local); Initial Catalog=VetDSD; Integrated Security=SSPI";

        public Mensaje Crear(Mensaje mensajeACrear)
        {
            Mensaje MensajeReg = null;
            Int32 ID = 0;

            string sql = "insert into mensaje(correo, mensaje) " +
                "values(@correo, @mensaje1); " +
                 "select cast(scope_identity() as int)";

            using (SqlConnection conexion = new SqlConnection(CadenaConexion))
            {
                conexion.Open();
                using (SqlCommand comando = new SqlCommand(sql, conexion))
                {
                    //comando.Parameters.Add(new SqlParameter("@ID", mensajeACrear.id));
                    comando.Parameters.Add(new SqlParameter("@correo", mensajeACrear.correo));
                    comando.Parameters.Add(new SqlParameter("@mensaje1", mensajeACrear.mensaje1));

                    ID = (Int32)comando.ExecuteScalar();
                }
                MensajeReg = Obtener(ID);
                return MensajeReg;
            }

        }
        public Mensaje Obtener(int ID)
        {
            Mensaje mensajeEncontrado = null;
            string sql = "select * from mensaje where id = @ID";

            using (SqlConnection conexion = new SqlConnection(CadenaConexion))
            {
                conexion.Open();
                using (SqlCommand comando = new SqlCommand(sql, conexion))
                {
                    comando.Parameters.Add(new SqlParameter("@ID", ID));

                    using (SqlDataReader resultado = comando.ExecuteReader())
                    {
                        if (resultado.Read())
                        {
                            mensajeEncontrado = new Mensaje()
                            {
                                id = (int)resultado["id"],
                                correo = (string)resultado["correo"],
                                mensaje1 = (string)resultado["mensaje"]
                            };
                        }
                    }
                }
                return mensajeEncontrado;
            }

        }

        public List<Mensaje> Listar()
        {

            List<Mensaje> mensajesEncontrados = new List<Mensaje>();
            Mensaje MensajeEncontrado = null;


            string sql = "select * from mensaje";

            using (SqlConnection conexion = new SqlConnection(CadenaConexion))
            {
                conexion.Open();
                using (SqlCommand comando = new SqlCommand(sql, conexion))
                {

                    using (SqlDataReader resultado = comando.ExecuteReader())
                    {
                        while (resultado.Read())
                        {
                            MensajeEncontrado = new Mensaje()
                            {
                                id = (int)resultado["id"],
                                correo = (string)resultado["correo"],
                                mensaje1 = (string)resultado["mensaje"]
                            };

                            mensajesEncontrados.Add(MensajeEncontrado);
                        }
                    }
                }
            }
            return mensajesEncontrados;

        }


    }
}