﻿using VetWSSOAP.Dominio;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;


namespace VetWSSOAP.Persistencia
{
    public class MascotaDAO
    {
        private string CadenaConexion = "Data Source=(local); Initial Catalog=VetDSD; Integrated Security=SSPI";

        public Mascota Crear(Mascota mascotaACrear)
        {
            Mascota MascotaReg = null;
            Int32 ID = 0;

            string sql = "insert into mascota(idcliente, nombre, fechaNacimiento, " +
                "idTipoMascota, idRazaMascota, Observaciones) " +
                "values(@idCliente, @nombre, @fechaNacimiento, " +
                "@idTipoMascota, @idRazaMascota, @Observaciones); " +
                 "select cast(scope_identity() as int)";

            using (SqlConnection conexion = new SqlConnection(CadenaConexion))
            {
                conexion.Open();
                using (SqlCommand comando = new SqlCommand(sql, conexion))
                {
                    comando.Parameters.Add(new SqlParameter("@idCliente", mascotaACrear.IDCliente));
                    comando.Parameters.Add(new SqlParameter("@nombre", mascotaACrear.Nombre));
                    comando.Parameters.Add(new SqlParameter("@fechaNacimiento", mascotaACrear.FechaNacimiento));
                    comando.Parameters.Add(new SqlParameter("@idTipoMascota", mascotaACrear.IDTipoMascota));
                    comando.Parameters.Add(new SqlParameter("@idRazaMascota", mascotaACrear.IDRazaMascota));
                    comando.Parameters.Add(new SqlParameter("@Observaciones", mascotaACrear.Observaciones));
                    
                    ID = (Int32)comando.ExecuteScalar();
                }
                MascotaReg = Obtener(ID);
                return MascotaReg;
            }

        }


        public Mascota Obtener(int IDMascota)
        {
            Mascota mascotaEncontrado = null;
            string sql = "select * from mascota where IDMascota = @IDMascota";

            using (SqlConnection conexion = new SqlConnection(CadenaConexion))
            {
                conexion.Open();
                using (SqlCommand comando = new SqlCommand(sql, conexion))
                {
                    comando.Parameters.Add(new SqlParameter("@IDMascota", IDMascota));

                    using (SqlDataReader resultado = comando.ExecuteReader())
                    {
                        if (resultado.Read())
                        {
                            mascotaEncontrado = new Mascota()
                            {
                                IDMascota = (int)resultado["IDMascota"],
                                IDCliente = (int)resultado["IDCliente"],
                                Nombre = (string)resultado["Nombre"],
                                FechaNacimiento = (DateTime)resultado["FechaNacimiento"],
                                IDTipoMascota = (int)resultado["IDTipoMascota"],
                                IDRazaMascota = (int)resultado["IDRazaMascota"],
                                Observaciones = (string)resultado["Observaciones"]
                            };
                        }
                    }
                }
                return mascotaEncontrado;
            }

        }


        public Mascota Modificar(Mascota mascotaAModificar)
        {

            Mascota MascotaReg = null;

            string sql = "update mascota set idcliente= @idCliente, nombre= @nombre," +
                " fechaNacimiento=@fechaNacimiento, idTipoMascota=@idTipoMascota, " +
                "idRazaMascota=@idRazaMascota, Observaciones=@Observaciones " +
                "where IDMascota=@IDMascota";

            using (SqlConnection conexion = new SqlConnection(CadenaConexion))
            {
                conexion.Open();
                using (SqlCommand comando = new SqlCommand(sql, conexion))
                {
                    comando.Parameters.Add(new SqlParameter("@idCliente", mascotaAModificar.IDCliente));
                    comando.Parameters.Add(new SqlParameter("@nombre", mascotaAModificar.Nombre));
                    comando.Parameters.Add(new SqlParameter("@fechaNacimiento", mascotaAModificar.FechaNacimiento));
                    comando.Parameters.Add(new SqlParameter("@idTipoMascota", mascotaAModificar.IDTipoMascota));
                    comando.Parameters.Add(new SqlParameter("@idRazaMascota", mascotaAModificar.IDRazaMascota));
                    comando.Parameters.Add(new SqlParameter("@Observaciones", mascotaAModificar.Observaciones));

                    comando.Parameters.Add(new SqlParameter("@IDMascota", mascotaAModificar.IDMascota));

                    comando.ExecuteNonQuery();

                }
                MascotaReg = Obtener(mascotaAModificar.IDMascota);
                return MascotaReg;
            }
        }
        
        
        public void Eliminar(int IDMascota)
        {

            string sql = "delete from mascota where IDMascota=@IDMascota";

            using (SqlConnection conexion = new SqlConnection(CadenaConexion))
            {
                conexion.Open();
                using (SqlCommand comando = new SqlCommand(sql, conexion))
                {
                    comando.Parameters.Add(new SqlParameter("@IDMascota", IDMascota));
                    comando.ExecuteNonQuery();
                }
            }
        }
        
        
        public List<Mascota> Listar()
        {

            List<Mascota> mascotasEncontrados = new List<Mascota>();
            Mascota MascotaEncontrado = null;


            string sql = "select * from mascota";

            using (SqlConnection conexion = new SqlConnection(CadenaConexion))
            {
                conexion.Open();
                using (SqlCommand comando = new SqlCommand(sql, conexion))
                {

                    using (SqlDataReader resultado = comando.ExecuteReader())
                    {
                        while (resultado.Read())
                        {
                            MascotaEncontrado = new Mascota()
                            {
                                IDMascota = (int)resultado["IDMascota"],
                                IDCliente = (int)resultado["IDCliente"],
                                Nombre = (string)resultado["Nombre"],
                                FechaNacimiento = (DateTime)resultado["FechaNacimiento"],
                                IDTipoMascota = (int)resultado["IDTipoMascota"],
                                IDRazaMascota = (int)resultado["IDRazaMascota"],
                                Observaciones = (string)resultado["Observaciones"]
                            };

                            mascotasEncontrados.Add(MascotaEncontrado);
                        }
                    }
                }
            }
            return mascotasEncontrados;

        }

    }
}
