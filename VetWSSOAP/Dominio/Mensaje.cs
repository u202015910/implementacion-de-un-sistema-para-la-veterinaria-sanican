﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace VetWSSOAP.Dominio
{
    [DataContract]
    public class Mensaje
    {

        [DataMember]
        public int id { get; set; }
        [DataMember]
        public string correo { get; set; }
        [DataMember]
        public string mensaje1 { get; set; }

    }
}