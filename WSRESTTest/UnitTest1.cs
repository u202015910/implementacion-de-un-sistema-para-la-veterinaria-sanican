﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Web.Script.Serialization;


namespace WSRESTTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestObtenerVenta()
        {
            DateTime freg = new DateTime(2020, 6, 25, 10, 0, 0);
            //DateTime fhas = new DateTime(2020, 6, 14, 17, 0, 0);

            //DateTime fdes = DateTime.SpecifyKind(fdes1, DateTimeKind.Utc);
            //DateTime fhas = DateTime.SpecifyKind(fhas1, DateTimeKind.Utc);

            HttpWebRequest request = (HttpWebRequest)WebRequest.
                Create("http://localhost:34974/VetService.svc/ventasvet/f001");
            request.Method = "GET";
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            StreamReader reader = new StreamReader(response.GetResponseStream());
            string tramaJson = reader.ReadToEnd();
            JavaScriptSerializer js = new JavaScriptSerializer();
            Venta ventaObtenido = js.Deserialize<Venta>(tramaJson);
            Assert.AreEqual("11", Convert.ToString(ventaObtenido.idVenta));
            Assert.AreEqual("f001", ventaObtenido.codigoventa);
            Assert.AreEqual("2", Convert.ToString(ventaObtenido.idcliente));
            Assert.AreEqual("2", Convert.ToString(ventaObtenido.idusuario));
            Assert.AreEqual(freg, ventaObtenido.fecha);
            //Assert.AreEqual(fhas, horarioObtenido.ffinal);
        }


        //[TestMethod]
        //public void TestObtenerHorario()
        //{
        //    DateTime fdes = new DateTime(2020, 6, 13, 15, 0, 0);
        //    DateTime fhas = new DateTime(2020, 6, 14, 17, 0, 0);

        //    //DateTime fdes = DateTime.SpecifyKind(fdes1, DateTimeKind.Utc);
        //    //DateTime fhas = DateTime.SpecifyKind(fhas1, DateTimeKind.Utc);

        //    HttpWebRequest request = (HttpWebRequest)WebRequest.
        //        Create("http://localhost:59410/PlatonService.svc/horarios/H20201MB01");
        //    request.Method = "GET";
        //    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
        //    StreamReader reader = new StreamReader(response.GetResponseStream());
        //    string tramaJson = reader.ReadToEnd();
        //    JavaScriptSerializer js = new JavaScriptSerializer();
        //    Horario horarioObtenido = js.Deserialize<Horario>(tramaJson);
        //    Assert.AreEqual("H20201MB01", horarioObtenido.codhorario);
        //    Assert.AreEqual("20201MB   ", horarioObtenido.codciclo);
        //    Assert.AreEqual("IS215 ", horarioObtenido.codcurso);
        //    Assert.AreEqual("T42B  ", horarioObtenido.codseccion);
        //    Assert.AreEqual(fdes, horarioObtenido.finicio);
        //    Assert.AreEqual(fhas, horarioObtenido.ffinal);
        //}
    }
}
