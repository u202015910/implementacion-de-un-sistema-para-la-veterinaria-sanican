﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WSRESTTest
{
    class Venta
    {
        public int idVenta { get; set; }
     
        public int idcliente { get; set; }
      
        public DateTime fecha { get; set; }
       
        public int idusuario { get; set; }
       
        public string codigoventa { get; set; }
    }
}
